package quickstart;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;

/**
 * Created by narozhny on 4/7/2016.
 */
public class HelloWorld {
    public static void main(String[] args) throws IOException {

        // Арифметические операторы
        int x = 2;
        int y = 4;
        boolean a = true;
        boolean b = false;

        System.out.println("Результат сложения = " + (x + y));
        System.out.println("Результат вычитания = " + (x - y));
        System.out.println("Результат умножения = " + x * y);
        System.out.println("Результат деления = " + y / x);

        // Операторы сравнения
        System.out.println(x > y);
        System.out.println(x < y);
        System.out.println(x <= y);
        System.out.println(x != y);

        // Логические операторы
        System.out.println(a || b);
        System.out.println(a && b);
        System.out.println(a ^ b);

        // Операторы приравнивания
        x += y;    //x = x + y;
        System.out.println(x);


        // Преобразование в строку
        int i = 10;
        double d = 10.1e23;
        long l = 9999999;
        float f = 3.14f;
        char ch = 'C';

        String str1 = Integer.toString(i);
        System.out.println(str1);

        String str2 = Double.toString(d);
        System.out.println(str2);

        String str3 = Long.toString(l);
        System.out.println(str3);

        String str4 = Float.toString(f);
        System.out.println(str4);

        String str5 = Character.toString(ch);
        System.out.println(str5);

        // Преобразование строки
        String str6 = "111";
        try {
            Byte b2 = Byte.valueOf(str6);  //str to byte
            System.out.println(b2);
        } catch (NumberFormatException e) {
            System.err.println("Неверный формат строки!");
        }

        String str7 = "59";
        try {
            Short s2 = Short.valueOf(str7);  //str to short
            System.out.println(s2);
        } catch (NumberFormatException e) {
            System.err.println("Неверный формат строки!");
        }

        String str8 = "1451";
        try {
            Integer i2 = Integer.valueOf(str8);  //str to int
            System.out.println(i2);
        } catch (NumberFormatException e) {
            System.err.println("Неверный формат строки!");
        }

        String str9 = "222222222";
        try {
            Long l2 = Long.valueOf(str9);  //str to long
            System.out.println(l2);
        } catch (NumberFormatException e) {
            System.err.println("Неверный формат строки!");
        }

        String str10 = "3.1415";
        try {
            Float f2 = Float.valueOf(str10);  //str to float
            System.out.println(f2);
        } catch (NumberFormatException e) {
            System.err.println("Неверный формат строки!");
        }

        String str11 = "564.6e10";
        try {
            Double d2 = Double.valueOf(str11);  //str to double
            System.out.println(d2);
        } catch (NumberFormatException e) {
            System.err.println("Неверный формат строки!");
        }

        String s1 = "True";
        String s2 = "yes";
        boolean boolean1, boolean2;

        boolean1 = Boolean.valueOf(s1); // str to boolean
        boolean2 = Boolean.valueOf(s2);
        System.out.println(boolean1);
        System.out.println(boolean2);


        // Перевод числа с точкой в тип без точки
        int bpt = 1000;
        float fl = (float) bpt;
        System.out.println(fl);

        float pt = 1000.11F;
        int i2 = (int) pt;
        System.out.println(i2 + "\n");


        // Ввод и математическая операция с двумя числами
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String z1;
        String z2;
        String z3;
        double var1;
        double var2;

        System.out.println("Первое число:");
        z1 = br.readLine();

        System.out.println("Математическая операция:");
        z2 = br.readLine();

        System.out.println("Второе число:");
        z3 = br.readLine();

        var1 = Double.valueOf(z1);
        var2 = Double.valueOf(z3);

        switch (z2) {

            case "-": {
                System.out.println("Результат вычитания: " + (var1 - var2));
                break;
            }

            case "+": {
                System.out.println("Результат сложения: " + (var1 + var2));
                break;
            }

            case "/": {
                System.out.println("Результат деления: " + (var1 / var2));
                break;
            }

            case "*": {
                System.out.println("Результат умножения: " + (var1 * var2));
                break;
            }

            default: {
                System.out.println("Введите корректный знак операции");
            }
        }

    }

}